const express = require('express')
const fs = require('fs');
const path = require('path')
const PORT = process.env.PORT || 5000
const cors = require('cors')

const app = express()

app.use(cors());

app.get('/products',(req,res)=>{
    const file = fs.readFileSync(path.join(__dirname, 'public/db.json' ))
    const products = JSON.parse(file)
    res.send(products)
})
.listen(PORT, () => console.log(`Listenings on ${ PORT }`))